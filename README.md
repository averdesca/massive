# Massive MovieDB Demo


A little sample on how to build a quick web app in ES6 and some other funny libraries.

## Installation
Download the source code from git repo
```Bash
    git clone https://averdesca@bitbucket.org/averdesca/massive.git
    cd ./massive
```

install npm dependencies
```Bash
    npm install
```
install bower dependencies
```Bash
    bower install
```



### Let's start
To start server and client just run (into the root path):
```Bash
    gulp serve
```


## Let's test

To start test please run:
```Bash
    gulp test
```

## Tecnologies
    
    -"node": ">=0.10.0"
    -"angular-animate": "~1.5.3"
    -"angular-cookies": "~1.5.3"
    -"angular-touch": "~1.5.3"
    -"angular-sanitize": "~1.5.3"
    -"angular-messages": "~1.5.3"
    -"angular-aria": "~1.5.3"
    -"jquery": "~2.1.4"
    -"angular-resource": "~1.5.3"
    -"angular-ui-router": "~0.2.15"
    -"bootstrap-sass": "~3.3.5"
    -"angular-bootstrap": "~0.14.3"
    -"malarkey": "yuanqing/malarkey#~1.3.1"
    -"angular-toastr": "~1.5.0"
    -"moment": "~2.10.6"
    -"animate.css": "~3.4.0"
    -"angular": "~1.5.3"
    -"estraverse": "~4.1.0"
    -"gulp": "~3.9.0"
    -"gulp-autoprefixer": "~3.0.2"
    -"gulp-angular-templatecache": "~1.8.0"
    -"del": "~2.0.2"
    -"lodash": "~3.10.1"
    -"gulp-cssnano": "~2.1.1"
    -"gulp-filter": "~3.0.1"
    -"gulp-flatten": "~0.2.0"
    -"gulp-eslint": "~1.0.0"
    -"eslint-plugin-angular": "~0.12.0"
    -"gulp-load-plugins": "~0.10.0"
    -"gulp-size": "~2.0.0"
    -"gulp-uglify": "~1.4.1"
    -"gulp-useref": "~3.0.3"
    -"gulp-util": "~3.0.6"
    -"gulp-replace": "~0.5.4"
    -"gulp-rename": "~1.2.2"
    -"gulp-rev": "~6.0.1"
    -"gulp-rev-replace": "~0.4.2"
    -"gulp-htmlmin": "~1.3.0"
    -"gulp-inject": "~3.0.0"
    -"gulp-protractor": "~2.1.0"
    -"gulp-sourcemaps": "~1.6.0"
    -"gulp-sass": "~2.0.4"
    -"webpack-stream": "~2.1.1"
    -"ng-annotate-loader": "0.0.10"
    -"eslint-loader": "~1.1.0"
    -"babel-core": "~6.7.4"
    -"babel-loader": "~6.2.4"
    -"babel-preset-es2015": "~6.6.0"
    -"main-bower-files": "~2.9.0"
    -"wiredep": "~2.2.2"
    -"karma": "~0.13.10"
    -"karma-jasmine": "~0.3.6"
    -"karma-phantomjs-launcher": "~0.2.1"
    -"phantomjs": "~1.9.18"
    -"karma-phantomjs-shim": "~1.2.0"
    -"karma-coverage": "~0.5.2"
    -"karma-ng-html2js-preprocessor": "~0.2.0"
    -"browser-sync": "~2.9.11"
    -"browser-sync-spa": "~1.0.3"
    -"http-proxy-middleware": "~0.9.0"
    -"chalk": "~1.1.1"
    -"uglify-save-license": "~0.4.1"
    -"wrench": "~1.5.8"


## License

Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
