/* global  moment:false */

import {theMovieDb} from '../lib/themoviedb/themoviedb';
import { config } from './index.config';
import { routerConfig } from './index.route';
import { runBlock } from './index.run';
import { MainController } from './main/main.controller';
import { GithubContributorService } from '../app/components/githubContributor/githubContributor.service';
// import { WebDevTecService } from '../app/components/webDevTec/webDevTec.service';
 import { NavbarDirective } from '../app/components/navbar/navbar.directive';
// import { MalarkeyDirective } from '../app/components/malarkey/malarkey.directive';

// service for searching movies
import { MovieDbSearchService } from '../app/components/movieDbSearch/movieDbSearch.service';

angular.module('movieDbdemo', ['ngAnimate', 'ngCookies', 'ngTouch', 'ngSanitize', 'ngMessages', 'ngAria', 'ngResource', 'ui.router', 'ui.bootstrap', 'toastr'])
  // .constant('malarkey', malarkey)
  .constant('moment', moment)
  .constant('theMovieDb',theMovieDb)
  .config(config)
  .config(routerConfig)
  .run(runBlock)
  .service('githubContributor', GithubContributorService)
  // .service('webDevTec', WebDevTecService)
  .service('movieDbSearch', MovieDbSearchService)
  .controller('MainController', MainController)
  .directive('acmeNavbar', NavbarDirective)
  // .directive('acmeMalarkey', MalarkeyDirective);
