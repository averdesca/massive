export function config ($logProvider, toastrConfig,theMovieDb ) {
  'ngInject';
  // Enable log
  $logProvider.debugEnabled(true);

  // Set options third-party lib
  toastrConfig.allowHtml = true;
  toastrConfig.timeOut = 3000;
  toastrConfig.positionClass = 'toast-top-right';
  toastrConfig.preventDuplicates = true;
  toastrConfig.progressBar = true;

  // theMovieDb api_key config
  theMovieDb.common.api_key = "176ec99131369ce2d315e38865bc181e";
}
