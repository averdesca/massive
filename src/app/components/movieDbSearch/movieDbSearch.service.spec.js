describe('service movieDbSearch', () => {
  beforeEach(angular.mock.module('movieDbdemo'));

  it('should be registered', inject(movieDbSearch => {
    expect(movieDbSearch).not.toEqual(null);
  }));

  describe('asyncSearch function', () => {
    it('should exist', inject(movieDbSearch => {
      expect(movieDbSearch.asyncSearch).not.toBeNull();
    }));

    it(', movieDbSearch should have api_key defined', inject(theMovieDb => {
      expect(theMovieDb.common.api_key).toEqual(jasmine.any(String));
    }));

    it('should return array of object', inject(movieDbSearch => {

      movieDbSearch.asyncSearch("ciao").then(function(response){
        var resultMap = response.results;


        expect(resultMap).toEqual(jasmine.any(Array));
        expect(resultMap[0]).toEqual(jasmine.any(Object));
        expect(resultMap.length > 5).toBeTruthy();
      });


    }));
  });
});
