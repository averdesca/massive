export class MovieDbSearchService {
  constructor ($q, $timeout, theMovieDb) {
    'ngInject';

    this.data = [];
    this._$q = $q;
    this.$timeout = $timeout;
    this.theMovieDb = theMovieDb;
  }


  /**
   * Asyncronously return an array of data using theMovieDb.search.getMovie() function.
   * @constructor
   * @param {string} name - the keyword to research.
   */
     asyncSearch(name) {
      var deferred = this._$q.defer();
      let scope = this;
      this.$timeout(function() {
        deferred.notify('starting query ' + name + '.');

        function successCB(data) {
          var dataObj = angular.fromJson(data);
          
          deferred.resolve(dataObj);
        }
        function errorCB() {
          deferred.reject();
        }

        return scope.theMovieDb.search.getMovie({"query": encodeURI(name)}, successCB, errorCB);
      }, 1000);

      return deferred.promise;
    }




}
