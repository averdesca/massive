describe('controllers', () => {
  let vm;

  beforeEach(angular.mock.module('movieDbdemo'));

  beforeEach(inject(($controller) => {
    
    vm = $controller('MainController');
  }));

  

  it('should define more than 5 awesome things', () => {
    expect(angular.isArray(vm.awesomeThings)).toBeTruthy();
   
  });

//#########################tests on  getMoviesByValue
describe('getMoviesByValue function', () => {

  it('should exist', () => {
  expect(vm.movieDbSearch).not.toBeNull();
});



it(', getMoviesByValue should return a not null Obj', () => {
  let prom = vm.getMoviesByValue('ciao');
    expect(prom).not.toBeNull();
    expect(prom).toEqual(jasmine.any(Object));
});

});

});
