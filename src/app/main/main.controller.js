export class MainController {
  constructor ($timeout,  movieDbSearch) {
    'ngInject';





    this.awesomeThings = [];


    this.movieDbSearch  =  movieDbSearch;
  }


  /**
   * Get movies by a value passed as parameter.
   * @constructor
   * @param {string} value - the keyword to research.
   */

  getMoviesByValue(value){

    return this.movieDbSearch.asyncSearch(value).then((function(response){
      var resultMap = response.results;
      // .map(function(item){ return item;      });
      this.awesomeThings = resultMap;
      return resultMap;
    }).bind(this));


  }
}
